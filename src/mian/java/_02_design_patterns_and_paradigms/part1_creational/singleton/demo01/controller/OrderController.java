package _02_design_patterns_and_paradigms.part1_creational.singleton.demo01.controller;


import _02_design_patterns_and_paradigms.part1_creational.singleton.demo01.Logger;
import _02_design_patterns_and_paradigms.part1_creational.singleton.demo01.vo.OrderVo;

import java.io.IOException;

public class OrderController {
    private Logger logger = new Logger();

    public OrderController() throws IOException {
    }

    public void create(OrderVo order) throws IOException {
        // ...省略业务逻辑代码...
        logger.log("Created an order: " + order.toString());
    }
}
