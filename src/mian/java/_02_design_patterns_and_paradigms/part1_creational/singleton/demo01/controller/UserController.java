package _02_design_patterns_and_paradigms.part1_creational.singleton.demo01.controller;


import _02_design_patterns_and_paradigms.part1_creational.singleton.demo01.Logger;

import java.io.IOException;

// Logger类的应用示例：
public class UserController {
    private Logger logger = new Logger();

    public UserController() throws IOException {
    }

    public void login(String username, String password) throws IOException {
        // ...省略业务逻辑代码...
        logger.log(username + " logined!");
    }
}
