package _02_design_patterns_and_paradigms.part1_creational.singleton.demo01;


import java.io.File;
import java.io.FileWriter;
import java.io.IOException;


//41|单例模式（上）：为什么说支持懒加载的双重检测不比饿汉式更优？

//示例1:处理资源访问冲突
// 自定义实现了一个往文件中打印日志的 Logger 类

//存在问题:
//所有的日志都写入到同一个文件 /Users/wangzheng/log.txt 中。
//在 UserController 和 OrderController 中，我们分别创建两个 Logger 对象。
//在 Web 容器的 Servlet 多线程环境下，如果两个 Servlet 线程同时分别执行 login() 和 create() 两个函数，并且同时写日志到 log.txt 文件中，那就有可能存在日志信息互相覆盖的情况。

//具体原因:
//为什么会出现互相覆盖呢？
//我们可以这么类比着理解。在多线程环境下，如果两个线程同时给同一个共享变量加 1，因为共享变量是竞争资源，所以，共享变量最后的结果有可能并不是加了 2，而是只加了 1。
//同理，这里的 log.txt 文件也是竞争资源，两个线程同时往里面写数据，就有可能存在互相覆盖的情况。

//解决方案:
//1. 加类级别的锁 demo02
//2. 使用单例模式
//3. 使用分布式锁...
//4. 使用并发队列(Java 中的 BlockingQueue)...


public class Logger {
    private FileWriter writer;

    public Logger() throws IOException {
        File file = new File("/Users/wangzheng/log.txt");
        writer = new FileWriter(file, true); //true表示追加写入
    }

    public void log(String message) throws IOException {
        writer.write(message);
    }
}

//TODO JDK中的设计模式




