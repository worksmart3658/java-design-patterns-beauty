package _01_design_principles_and_ideas.part2_design_principles.demo05;

//19|理论五：控制反转、依赖反转、依赖注入，这三者有何区别和联系？

//控制反转 示例一: 常规写法
public class UserServiceTest {
    public static boolean doTest() {
        // ...
        return false;
    }

    public static void main(String[] args) {//这部分逻辑可以放到框架中
        if (doTest()) {
            System.out.println("Test succeed.");
        } else {
            System.out.println("Test failed.");
        }
    }
}
