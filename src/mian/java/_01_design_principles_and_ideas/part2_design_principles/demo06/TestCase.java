package _01_design_principles_and_ideas.part2_design_principles.demo06;


//抽象出一个框架

public abstract class TestCase {
    public void run() {
        if (doTest()) {
            System.out.println("Test succeed.");
        } else {
            System.out.println("Test failed.");
        }
    }

    public abstract boolean doTest();
}


