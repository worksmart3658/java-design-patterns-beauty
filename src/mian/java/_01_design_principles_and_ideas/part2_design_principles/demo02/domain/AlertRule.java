package _01_design_principles_and_ideas.part2_design_principles.demo02.domain;

/**
 * @ClassName AlertRule
 * @Description 告警规则
 * @Author zhanghuaxing
 * @Date 11/14/2020 2:55 PM
 **/
public class AlertRule {

    private int maxTps;

    public int getMaxTps() {
        return maxTps;
    }

    public AlertRule getMatchedRule(String api) {

        return this;
    }

    public long getMaxErrorCount() {
        return 0;
    }
}
