package _01_design_principles_and_ideas.part2_design_principles.demo02.domain;

import _01_design_principles_and_ideas.part2_design_principles.demo02.domain.eums.NotificationEmergencyLevel;

/**
 * @ClassName Notification
 * @Description 提醒
 * @Author zhanghuaxing
 * @Date 11/14/2020 2:56 PM
 **/
public class Notification {

    public void notify(NotificationEmergencyLevel urgency, String s) {

    }
}
