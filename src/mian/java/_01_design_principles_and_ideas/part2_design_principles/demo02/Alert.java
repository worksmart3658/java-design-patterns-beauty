package _01_design_principles_and_ideas.part2_design_principles.demo02;


//15|理论一：对于单一职责原则，如何判定某个类的职责是否够“单一”？
//开闭原则的英文全称是 Open Closed Principle，简写为 OCP。

//API 接口监控告警示例代码


//功能详情:
//AlertRule 存储告警规则，可以自由设置。
//Notification 是告警通知类，支持邮件、短信、微信、手机等多种通知渠道。
//NotificationEmergencyLevel 表示通知的紧急程度，
//      包括 SEVERE（严重）、URGENCY（紧急）、NORMAL（普通）、TRIVIAL（无关紧要），不同的紧急程度对应不同的发送渠道。


import _01_design_principles_and_ideas.part2_design_principles.demo02.domain.AlertRule;
import _01_design_principles_and_ideas.part2_design_principles.demo02.domain.Notification;
import _01_design_principles_and_ideas.part2_design_principles.demo02.domain.eums.NotificationEmergencyLevel;

public class Alert {
    private AlertRule rule;
    private Notification notification;

    public Alert(AlertRule rule, Notification notification) {
        this.rule = rule;
        this.notification = notification;
    }

    /**
     * 检查
     * 当接口的 TPS 超过某个预先设置的最大值时，以及当接口请求出错数大于某个最大允许值时，
     * 就会触发告警，通知接口的相关负责人或者团队。
     * @param api
     * @param requestCount
     * @param errorCount
     * @param durationOfSeconds
     */
    public void check(String api, long requestCount, long errorCount, long durationOfSeconds) {
        long tps = requestCount / durationOfSeconds;
        if (tps > rule.getMatchedRule(api).getMaxTps()) {
            notification.notify(NotificationEmergencyLevel.URGENCY, "...");
        }
        if (errorCount > rule.getMatchedRule(api).getMaxErrorCount()) {
            notification.notify(NotificationEmergencyLevel.SEVERE, "...");
        }
    }
}