##`16|理论二：如何做到“对扩展开放、修改关闭”？`
- 要点1：开闭原则”的设计初衷：只要它没有破坏原有的代码的正常运行，没有破坏原有的单元测试，我们就可以说，这是一个合格的代码改动。

- 要点2：添加一个新功能，不可能任何模块、类、方法的代码都不“修改”。我们要做的是尽量让修改操作更集中、更少、更上层，尽量让最核心、最复杂的那部分逻辑代码满足开闭原则。

- 要点3：最常用来提高代码扩展性的方法有：多态、依赖注入、基于接口而非实现编程，以及大部分的设计模式（比如，装饰、策略、模板、职责链、状态）。