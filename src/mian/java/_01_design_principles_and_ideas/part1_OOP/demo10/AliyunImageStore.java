package _01_design_principles_and_ideas.part1_OOP.demo10;

import _01_design_principles_and_ideas.part1_OOP.demo02.entity.Image;

//09|理论六：基于接口而非实现编程
//示例:
// 整个上传流程包含三个步骤：
//  创建 bucket（你可以简单理解为存储目录）、
//  生成 access token 访问凭证、
//  携带 access token 上传图片到指定的 bucket 中。

//当前实现方式存在定制化问题

public class AliyunImageStore {
    //...省略属性、构造函数等...

    public void createBucketIfNotExisting(String bucketName) {
        // ...创建bucket代码逻辑...
        // ...失败会抛出异常..
    }

    public String generateAccessToken() {
        // ...根据accesskey/secrectkey等生成access token
        return null;
    }

    public String uploadToAliyun(Image image, String bucketName, String accessToken) {
        //...上传图片到阿里云...
        //...返回图片存储在阿里云上的地址(url）...
        return null;
    }

    public Image downloadFromAliyun(String url, String accessToken) {
        //...从阿里云下载图片...
        return null;
    }
}


