package _01_design_principles_and_ideas.part1_OOP.demo15;

//10|理论七：多用组合少用继承
//  Crawler 类和 PageAnalyzer 类，它们都用到了 URL 拼接和分割的功能，
//  但并不具有继承关系（既不是父子关系，也不是兄弟关系）。
//  这个时候，使用组合就更加合理、更加灵活。

public class Url {
    //...省略属性和方法
}




