package _01_design_principles_and_ideas.part1_OOP.demo09;


import _01_design_principles_and_ideas.part1_OOP.demo09.base.RpcException;
import _01_design_principles_and_ideas.part1_OOP.demo09.base.RpcRequest;

//08|理论五：接口vs抽象类

// 接口
public interface Filter {
    void doFilter(RpcRequest req) throws RpcException;
}



