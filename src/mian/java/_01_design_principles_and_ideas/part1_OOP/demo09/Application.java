package _01_design_principles_and_ideas.part1_OOP.demo09;

import _01_design_principles_and_ideas.part1_OOP.demo09.base.RpcException;
import _01_design_principles_and_ideas.part1_OOP.demo09.base.RpcRequest;
import _01_design_principles_and_ideas.part1_OOP.demo09.impl.AuthencationFilter;
import _01_design_principles_and_ideas.part1_OOP.demo09.impl.RateLimitFilter;

import java.util.ArrayList;
import java.util.List;

// 过滤器使用Demo
public class Application {

    private static List<Filter> filters = new ArrayList<>();

    public void handleRpcRequest(RpcRequest req) {
        try {
            for (Filter filter : filters) {
                filter.doFilter(req);
            }
        } catch (RpcException e) {
            // ...处理过滤结果...
        }
        // ...省略其他处理逻辑...
    }

    public static void main(String[] args) {
        filters.add(new AuthencationFilter());
        filters.add(new RateLimitFilter());
        Application application = new Application();
        application.handleRpcRequest(new RpcRequest());
    }
}
