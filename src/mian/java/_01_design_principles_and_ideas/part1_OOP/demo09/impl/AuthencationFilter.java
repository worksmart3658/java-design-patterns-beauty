package _01_design_principles_and_ideas.part1_OOP.demo09.impl;

import _01_design_principles_and_ideas.part1_OOP.demo09.Filter;
import _01_design_principles_and_ideas.part1_OOP.demo09.base.RpcException;
import _01_design_principles_and_ideas.part1_OOP.demo09.base.RpcRequest;

// 接口实现类：鉴权过滤器
public class AuthencationFilter implements Filter {
    @Override
    public void doFilter(RpcRequest req) throws RpcException {
        //...鉴权逻辑..
        System.out.println("鉴权逻辑");
    }
}
