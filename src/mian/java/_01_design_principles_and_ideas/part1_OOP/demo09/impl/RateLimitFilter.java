package _01_design_principles_and_ideas.part1_OOP.demo09.impl;

import _01_design_principles_and_ideas.part1_OOP.demo09.Filter;
import _01_design_principles_and_ideas.part1_OOP.demo09.base.RpcException;
import _01_design_principles_and_ideas.part1_OOP.demo09.base.RpcRequest;

// 接口实现类：限流过滤器
public class RateLimitFilter implements Filter {
    @Override
    public void doFilter(RpcRequest req) throws RpcException {
        //...限流逻辑...
        System.out.println("限流逻辑");
    }
}
