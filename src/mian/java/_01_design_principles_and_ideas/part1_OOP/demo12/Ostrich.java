package _01_design_principles_and_ideas.part1_OOP.demo12;

public class Ostrich extends AbstractBird { //鸵鸟
    //...省略其他属性和方法...
    public void fly() throws UnSupportedMethodException {
        throw new UnSupportedMethodException("I can't fly.'");
    }
}
