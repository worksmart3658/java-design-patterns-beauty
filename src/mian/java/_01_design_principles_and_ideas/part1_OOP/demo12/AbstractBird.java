package _01_design_principles_and_ideas.part1_OOP.demo12;

//10|理论七：多用组合少用继承
//示例: 反例,鸵鸟继承类抽象鸟的fly方法
public class AbstractBird {
    //...省略其他属性和方法...
    public void fly() throws UnSupportedMethodException {
        //...
    }
}


