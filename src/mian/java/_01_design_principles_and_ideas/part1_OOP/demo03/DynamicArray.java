package _01_design_principles_and_ideas.part1_OOP.demo03;


//05|理论二：封装、抽象、继承、多态
//当前的类的创建是为了解释: 多态（Polymorphism）
// 子类可以替换父类，在实际的代码运行过程中，调用子类的方法实现
// 当前通过继承实现多态

//定义动态数组类
public class DynamicArray {
    private static final int DEFAULT_CAPACITY = 10;
    protected int size = 0;
    protected int capacity = DEFAULT_CAPACITY;
    protected Integer[] elements = new Integer[DEFAULT_CAPACITY];

    public int size() {
        return this.size;
    }

    public Integer get(int index) {
        return elements[index];
    }
    //...省略n多方法...

    public void add(Integer e) {
        ensureCapacity();
        elements[size++] = e;
    }

    protected void ensureCapacity() {
        //...如果数组满了就扩容...代码省略...
    }
}



