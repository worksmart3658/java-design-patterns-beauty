package _01_design_principles_and_ideas.part1_OOP.demo04;


//05|理论二：封装、抽象、继承、多态
//当前的类的创建是为了解释: 多态（Polymorphism）
// 子类可以替换父类，在实际的代码运行过程中，调用子类的方法实现
// 当前通过接口类实现多态
public interface Iterator {
    boolean hasNext();

    String next();

    String remove();
}
