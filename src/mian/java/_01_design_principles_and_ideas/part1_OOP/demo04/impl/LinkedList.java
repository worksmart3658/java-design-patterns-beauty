package _01_design_principles_and_ideas.part1_OOP.demo04.impl;

import _01_design_principles_and_ideas.part1_OOP.demo04.Iterator;
import _01_design_principles_and_ideas.part1_OOP.demo04.entity.LinkedListNode;

public class LinkedList implements Iterator {
    private LinkedListNode head;

    public boolean hasNext() {
        //具体实现
        return true;
    }

    public String next() {
        //具体实现
        return null;
    }

    public String remove() {
        //具体实现
        return null;
    }
    //...省略其他方法...
}
