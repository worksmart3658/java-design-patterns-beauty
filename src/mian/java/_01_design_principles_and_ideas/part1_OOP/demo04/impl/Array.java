package _01_design_principles_and_ideas.part1_OOP.demo04.impl;

import _01_design_principles_and_ideas.part1_OOP.demo04.Iterator;

public class Array implements Iterator {
    private String[] data;

    public boolean hasNext() {
        //具体实现
        return false;
    }

    public String next() {
        //具体实现
        return null;
    }

    public String remove() {
        //具体实现
        return null;
    }
    //...省略其他方法...
}