package _01_design_principles_and_ideas.part1_OOP.demo04;

import _01_design_principles_and_ideas.part1_OOP.demo04.impl.Array;
import _01_design_principles_and_ideas.part1_OOP.demo04.impl.LinkedList;

public class Demo {
    private static void print(Iterator iterator) {
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }

    public static void main(String[] args) {
        Iterator arrayIterator = new Array();
        print(arrayIterator);

        Iterator linkedListIterator = new LinkedList();
        print(linkedListIterator);
    }
}

