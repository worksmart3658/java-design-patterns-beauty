package _01_design_principles_and_ideas.part1_OOP.demo01;


import java.math.BigDecimal;

//05|理论二：封装、抽象、继承、多态
//当前的类的创建是为了解释: 封装
// 对钱包的这四个属性的访问方式进行了限制
// 只允许通过下面这六个方法来访问或者修改钱包里的数据
public class Wallet {
    private String id;
    private long createTime;
    private BigDecimal balance;
    private long balanceLastModifiedTime;
    // ...省略其他属性...

    public Wallet() {
        //this.id = IdGenerator.getInstance().generate();
        this.id = "";
        this.createTime = System.currentTimeMillis();
        this.balance = BigDecimal.ZERO;
        this.balanceLastModifiedTime = System.currentTimeMillis();
    }

    // 注意：下面对get方法做了代码折叠，是为了减少代码所占文章的篇幅
    public String getId() {
        return this.id;
    }

    public long getCreateTime() {
        return this.createTime;
    }

    public BigDecimal getBalance() {
        return this.balance;
    }

    public long getBalanceLastModifiedTime() {
        return this.balanceLastModifiedTime;
    }

    public void increaseBalance(BigDecimal increasedAmount) throws InvalidAmountException {
        if (increasedAmount.compareTo(BigDecimal.ZERO) < 0) {
            throw new InvalidAmountException("...");
        }
        this.balance.add(increasedAmount);
        this.balanceLastModifiedTime = System.currentTimeMillis();
    }

    public void decreaseBalance(BigDecimal decreasedAmount) throws InvalidAmountException, InsufficientAmountException {
        if (decreasedAmount.compareTo(BigDecimal.ZERO) < 0) {
            throw new InvalidAmountException("...");
        }
        if (decreasedAmount.compareTo(this.balance) > 0) {
            throw new InsufficientAmountException("...");
        }
        this.balance.subtract(decreasedAmount);
        this.balanceLastModifiedTime = System.currentTimeMillis();
    }

    private class InvalidAmountException extends Throwable {
        public InvalidAmountException(String s) {
        }
    }

    private class InsufficientAmountException extends Throwable {
        public InsufficientAmountException(String s) {
        }
    }
}
