package _01_design_principles_and_ideas.part1_OOP.demo18.service;

public enum Status {
    TO_BE_EXECUTED,
    CLOSED,
    FAILED,
    EXECUTED,
    ;
}
