package _01_design_principles_and_ideas.part1_OOP.demo18.controller;


import _01_design_principles_and_ideas.part1_OOP.demo18.service.VirtualWalletService;

import java.math.BigDecimal;

//12|实战一（下）：如何利用基于充血模型的DDD开发一个虚拟钱包系统？
//示例: 基于贫血.
//      利用基于贫血模型的传统开发模式来实现的虚拟钱包系统

public class VirtualWalletController {
    // 通过构造函数或者IOC框架注入
    private VirtualWalletService virtualWalletService;

    //查询余额
    public BigDecimal getBalance(Long walletId) {
        return null;
    }

    //出账
    public void debit(Long walletId, BigDecimal amount) {
    }

    //入账
    public void credit(Long walletId, BigDecimal amount) {
    }

    //转账
    public void transfer(Long fromWalletId, Long toWalletId, BigDecimal amount) {
    }
}
