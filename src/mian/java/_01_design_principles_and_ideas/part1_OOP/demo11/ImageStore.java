package _01_design_principles_and_ideas.part1_OOP.demo11;


import _01_design_principles_and_ideas.part1_OOP.demo02.entity.Image;

//09|理论六：基于接口而非实现编程
//示例:图片上传功能重构


public interface ImageStore {
    String upload(Image image, String bucketName);

    Image download(String url);
}






