package _01_design_principles_and_ideas.part1_OOP.demo11.impl;

import _01_design_principles_and_ideas.part1_OOP.demo02.entity.Image;
import _01_design_principles_and_ideas.part1_OOP.demo11.ImageStore;

// 上传下载流程改变：私有云不需要支持access token
public class PrivateImageStore implements ImageStore {
    public String upload(Image image, String bucketName) {
        createBucketIfNotExisting(bucketName);
        //...上传图片到私有云...
        //...返回图片的url...
        return null;
    }

    public Image download(String url) {
        //...从私有云下载图片...
        return null;
    }

    private void createBucketIfNotExisting(String bucketName) {
        // ...创建bucket...
        // ...失败会抛出异常..
    }
}
