package _01_design_principles_and_ideas.part1_OOP.demo13;

import _01_design_principles_and_ideas.part1_OOP.demo13.able.EggLayable;
import _01_design_principles_and_ideas.part1_OOP.demo13.able.Tweetable;

//10|理论七：多用组合少用继承
//示例: 鸵鸟实现: 鸣叫接口类, 下蛋接口类
//  通过实现接口类方法, 来组合功能

//鸵鸟
public class Ostrich implements Tweetable, EggLayable {

    //... 省略其他属性和方法...
    @Override
    public void tweet() {
        //...
    }

    @Override
    public void layEgg() {
        //...
    }
}
