package _01_design_principles_and_ideas.part1_OOP.demo13.able;

public interface Tweetable {
    void tweet();
}
