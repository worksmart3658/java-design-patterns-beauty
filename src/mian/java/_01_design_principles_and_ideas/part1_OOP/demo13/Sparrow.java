package _01_design_principles_and_ideas.part1_OOP.demo13;

import _01_design_principles_and_ideas.part1_OOP.demo13.able.EggLayable;
import _01_design_principles_and_ideas.part1_OOP.demo13.able.Flyable;
import _01_design_principles_and_ideas.part1_OOP.demo13.able.Tweetable;

//麻雀
public class Sparrow implements Flyable, Tweetable, EggLayable {

    //... 省略其他属性和方法...
    @Override
    public void fly() { //...
    }

    @Override
    public void tweet() { //...
    }

    @Override
    public void layEgg() { //...
    }
}
