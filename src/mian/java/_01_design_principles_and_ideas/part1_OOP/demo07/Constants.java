package _01_design_principles_and_ideas.part1_OOP.demo07;

//07|理论四：哪些代码设计看似是面向对象，实际是面向过程的？
//当前的类的创建是为了解释:  反例
// 滥用全局变量和全局方法
// 查找修改某个常量也会变得比较费时，而且还会增加提交代码冲突的概率
// 增加代码的编译时间
// 影响代码的复用性,引入了很多无关的常量到新的项目中

//解决方案:
// 第一种是将 Constants 类拆解为功能更加单一的多个类，比如跟 MySQL 配置相关的常量，我们放到 MysqlConstants 类中
// 还有一种我个人觉得更好的设计思路，那就是并不单独地设计 Constants 常量类，而是哪个类用到了某个常量，我们就把这个常量定义到这个类中

public class Constants {
    public static final String MYSQL_ADDR_KEY = "mysql_addr";
    public static final String MYSQL_DB_NAME_KEY = "db_name";
    public static final String MYSQL_USERNAME_KEY = "mysql_username";
    public static final String MYSQL_PASSWORD_KEY = "mysql_password";

    public static final String REDIS_DEFAULT_ADDR = "192.168.7.2:7234";
    public static final int REDIS_DEFAULT_MAX_TOTAL = 50;
    public static final int REDIS_DEFAULT_MAX_IDLE = 50;
    public static final int REDIS_DEFAULT_MIN_IDLE = 20;
    public static final String REDIS_DEFAULT_KEY_PREFIX = "rt:";

    // ...省略更多的常量定义...
}
