package _01_design_principles_and_ideas.part1_OOP.demo08.logextend;

import _01_design_principles_and_ideas.part1_OOP.demo08.Logger;
import _01_design_principles_and_ideas.part1_OOP.demo08.client.MessageQueueClient;

import java.util.logging.Level;

// 抽象类的子类: 输出日志到消息中间件(比如kafka)
public class MessageQueueLogger extends Logger {
    private MessageQueueClient msgQueueClient;

    public MessageQueueLogger(String name, boolean enabled,
                              Level minPermittedLevel, MessageQueueClient msgQueueClient) {
        super(name, enabled, minPermittedLevel);
        this.msgQueueClient = msgQueueClient;
    }

    @Override
    protected void doLog(Level level, String mesage) {
        // 格式化level和message,输出到消息中间件
        msgQueueClient.send("...");
    }
}
