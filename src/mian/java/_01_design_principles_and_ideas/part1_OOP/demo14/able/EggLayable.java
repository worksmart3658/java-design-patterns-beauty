package _01_design_principles_and_ideas.part1_OOP.demo14.able;

public interface EggLayable {
    void layEgg();
}
