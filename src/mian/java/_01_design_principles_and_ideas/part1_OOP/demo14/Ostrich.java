package _01_design_principles_and_ideas.part1_OOP.demo14;

import _01_design_principles_and_ideas.part1_OOP.demo14.able.EggLayable;
import _01_design_principles_and_ideas.part1_OOP.demo14.able.Tweetable;
import _01_design_principles_and_ideas.part1_OOP.demo14.able.impl.EggLayAbility;
import _01_design_principles_and_ideas.part1_OOP.demo14.able.impl.TweetAbility;
//10|理论七：多用组合少用继承
//示例: 鸵鸟实现: 鸣叫接口类, 下蛋接口类
//  对于公共的实现功能,针对三个接口再定义三个实现类,
//  通过组合和委托技术来消除代码重复。

public class Ostrich implements Tweetable, EggLayable {//鸵鸟

    private TweetAbility tweetAbility = new TweetAbility(); //组合
    private EggLayAbility eggLayAbility = new EggLayAbility(); //组合

    //... 省略其他属性和方法...
    @Override
    public void tweet() {
        tweetAbility.tweet(); // 委托
    }

    @Override
    public void layEgg() {
        eggLayAbility.layEgg(); // 委托
    }
}
