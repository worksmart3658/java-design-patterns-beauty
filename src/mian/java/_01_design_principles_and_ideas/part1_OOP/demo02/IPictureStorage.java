package _01_design_principles_and_ideas.part1_OOP.demo02;


import _01_design_principles_and_ideas.part1_OOP.demo02.entity.Image;
import _01_design_principles_and_ideas.part1_OOP.demo02.entity.Picture;
import _01_design_principles_and_ideas.part1_OOP.demo02.entity.PictureMetaInfo;


//05|理论二：封装、抽象、继承、多态
//当前的类的创建是为了解释: 抽象
// 不需要去研究函数内部的实现逻辑，
// 只需要通过函数的命名、注释或者文档，了解其提供的功能，就可以直接使用
public interface IPictureStorage {

    void savePicture(Picture picture);

    Image getPicture(String pictureId);

    void deletePicture(String pictureId);

    void modifyMetaInfo(String pictureId, PictureMetaInfo metaInfo);
}


