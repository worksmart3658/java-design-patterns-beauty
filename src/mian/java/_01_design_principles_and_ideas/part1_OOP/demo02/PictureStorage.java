package _01_design_principles_and_ideas.part1_OOP.demo02;

import _01_design_principles_and_ideas.part1_OOP.demo02.entity.Image;
import _01_design_principles_and_ideas.part1_OOP.demo02.entity.Picture;
import _01_design_principles_and_ideas.part1_OOP.demo02.entity.PictureMetaInfo;

public class PictureStorage implements IPictureStorage {
    // ...省略其他属性...
    @Override
    public void savePicture(Picture picture) {
        //具体实现
    }

    @Override
    public Image getPicture(String pictureId) {
        //具体实现
        return null;
    }

    @Override
    public void deletePicture(String pictureId) {
        //具体实现
    }

    @Override
    public void modifyMetaInfo(String pictureId, PictureMetaInfo metaInfo) {
        //具体实现
    }
}
