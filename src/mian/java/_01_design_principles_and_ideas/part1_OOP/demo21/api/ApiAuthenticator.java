package _01_design_principles_and_ideas.part1_OOP.demo21.api;

import _01_design_principles_and_ideas.part1_OOP.demo21.domain.ApiRequest;

/**
 * @ClassName ApiAuthenticator
 * @Description api认证器
 * @Author zhanghuaxing
 * @Date 11/10/2020 1:13 PM
 **/

//14|实战二（下）：如何利用面向对象设计和编程开发接口鉴权功能？
//实例: 设计一个API鉴权的结构实现.面向对象设计, 充血模式.

public interface ApiAuthenticator {
    void auth(String url);

    void auth(ApiRequest apiRequest);
}


