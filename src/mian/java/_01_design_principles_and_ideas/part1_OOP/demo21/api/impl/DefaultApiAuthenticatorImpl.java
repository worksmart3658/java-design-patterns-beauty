package _01_design_principles_and_ideas.part1_OOP.demo21.api.impl;

import _01_design_principles_and_ideas.part1_OOP.demo21.api.ApiAuthenticator;
import _01_design_principles_and_ideas.part1_OOP.demo21.domain.ApiRequest;
import _01_design_principles_and_ideas.part1_OOP.demo21.domain.AuthToken;
import _01_design_principles_and_ideas.part1_OOP.demo21.service.CredentialStorage;
import _01_design_principles_and_ideas.part1_OOP.demo21.service.impl.MysqlCredentialStorage;

/**
 * @ClassName DefaultApiAuthenticatorImpl
 * @Description 实现类
 * @Author zhanghuaxing
 * @Date 11/10/2020 1:15 PM
 **/
public class DefaultApiAuthenticatorImpl implements ApiAuthenticator {

    private CredentialStorage credentialStorage;

    public DefaultApiAuthenticatorImpl() {
        this.credentialStorage = new MysqlCredentialStorage();
    }

    public DefaultApiAuthenticatorImpl(CredentialStorage credentialStorage) {
        this.credentialStorage = credentialStorage;
    }

    @Override
    public void auth(String url) {
        ApiRequest apiRequest = ApiRequest.buildFromUrl(url);
        auth(apiRequest);
    }

    @Override
    public void auth(ApiRequest apiRequest) {
        String appId = apiRequest.getAppId();
        String token = apiRequest.getToken();
        long timestamp = apiRequest.getTimestamp();
        String originalUrl = apiRequest.getOriginalUrl();

        AuthToken clientAuthToken = new AuthToken(token, timestamp);
        if (clientAuthToken.isExpired()) {
            throw new RuntimeException("Token is expired.");
        }

        String password = credentialStorage.getPasswordByAppId(appId);
        AuthToken serverAuthToken = AuthToken.generate(originalUrl, appId, password, timestamp);
        if (!serverAuthToken.match(clientAuthToken)) {
            throw new RuntimeException("Token verfication failed.");
        }
    }
}
