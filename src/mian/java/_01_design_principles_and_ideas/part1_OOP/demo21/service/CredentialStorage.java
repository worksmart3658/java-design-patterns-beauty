package _01_design_principles_and_ideas.part1_OOP.demo21.service;

/**
 * @ClassName CredentialStorage
 * @Description 证书存储
 * @Author zhanghuaxing
 * @Date 11/10/2020 1:16 PM
 **/
public interface CredentialStorage {

    String getPasswordByAppId(String appId);
}
