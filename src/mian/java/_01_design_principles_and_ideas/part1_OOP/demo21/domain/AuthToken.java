package _01_design_principles_and_ideas.part1_OOP.demo21.domain;

/**
 * @ClassName AuthToken
 * @Description 认证token
 * @Author zhanghuaxing
 * @Date 11/10/2020 1:20 PM
 **/
public class AuthToken {

    private static final long DEFAULT_EXPIRED_TIME_INTERVAL = 1 * 60 * 1000;
    private String token;
    private long createTime;
    private long expiredTimeInterval = DEFAULT_EXPIRED_TIME_INTERVAL;


    public AuthToken(String token, long createTime) {
        this.token =token;
        this.createTime = createTime;
    }

    public AuthToken(String token, long createTime,long expiredTimeInterval) {
        this.token =token;
        this.createTime = createTime;
        this.expiredTimeInterval = expiredTimeInterval;
    }

    public static AuthToken generate(String baseUrl, String appId, String password, long timestamp) {
        return null;
    }

    public boolean isExpired() {
        return false;
    }

    public boolean match(AuthToken clientAuthToken) {
        return false;
    }
}
