package _01_design_principles_and_ideas.part1_OOP.demo21.domain;

/**
 * @ClassName ApiRequest
 * @Description API请求对象
 * @Author zhanghuaxing
 * @Date 11/10/2020 1:15 PM
 **/
public class ApiRequest {

    private String baseUrl;
    private String token;
    private String appId;
    private Long timestamp;

    public static ApiRequest buildFromUrl(String url) {
        //TODO 此处有疑惑,静态方法,似乎不行
        return null;
    }

    public String getAppId() {
        return null;
    }

    public String getToken() {
        return null;
    }

    public long getTimestamp() {
        return 0;
    }

    public String getOriginalUrl() {
        return null;
    }
}
