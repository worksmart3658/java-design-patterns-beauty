package _01_design_principles_and_ideas.part1_OOP.demo16;


public class FeignClient { // Feign Client框架代码
    //...省略其他代码...
    public void encode(String url) { //...
    }

    public void demofunction(FeignClient feignClient) {
        //...
        feignClient.encode("url");
        //...
    }
}


