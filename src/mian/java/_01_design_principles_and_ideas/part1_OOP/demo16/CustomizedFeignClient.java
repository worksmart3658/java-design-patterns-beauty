package _01_design_principles_and_ideas.part1_OOP.demo16;


//10|理论七：多用组合少用继承
//示例:如果你不能改变一个函数的入参类型，而入参又非接口，为了支持多态，只能采用继承来实现。
// 比如 FeignClient 是一个外部类，我们没有权限去修改这部分代码，
// 但是我们希望能重写这个类在运行时执行的 encode() 函数。
// 这个时候，我们只能采用继承来实现了。
public class CustomizedFeignClient extends FeignClient {
    @Override
    public void encode(String url) {
        //...重写encode的实现...
    }

    void doSomething() {
        // 调用
        FeignClient client = new CustomizedFeignClient();
        demofunction(client);
    }
}
