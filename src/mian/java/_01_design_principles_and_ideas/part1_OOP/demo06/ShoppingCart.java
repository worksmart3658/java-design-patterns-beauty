package _01_design_principles_and_ideas.part1_OOP.demo06;


//07|理论四：哪些代码设计看似是面向对象，实际是面向过程的？
//当前的类的创建是为了解释: 使用面向对象语言编写面向过程的代码示例   反例
// 滥用 getter、setter 方法

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {
    private int itemsCount;
    private double totalPrice;
    private List<ShoppingCartItem> items = new ArrayList<>();

    public int getItemsCount() {
        return this.itemsCount;
    }

    public void setItemsCount(int itemsCount) {
        this.itemsCount = itemsCount;
    }

    public double getTotalPrice() {
        return this.totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public List<ShoppingCartItem> getItems() {
        return this.items;
    }

    public void addItem(ShoppingCartItem item) {
        items.add(item);
        itemsCount++;
        totalPrice += item.getPrice();
    }
    // ...省略其他方法...
}
