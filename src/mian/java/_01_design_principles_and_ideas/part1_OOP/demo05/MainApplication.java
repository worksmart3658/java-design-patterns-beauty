package _01_design_principles_and_ideas.part1_OOP.demo05;


//06|理论三：面向对象与面向过程
//当前的类的创建是为了解释: 面向对象的形式
// 面向过程和面向对象最基本的区别就是，代码的组织方式不同。
// 面向过程风格的代码被组织成了一组方法集合及其数据结构（struct User），方法和数据结构的定义是分开的。
// 面向对象风格的代码被组织成一组类，方法和数据结构被绑定一起，定义在类中。
// 个人认为这个定义没多大意义.......


public class MainApplication {
    public static void main(String[] args) {
        UserFileFormatter userFileFormatter = new UserFileFormatter();
        userFileFormatter.format("/home/zheng/users.txt", "/home/zheng/formatted_users.txt");
    }
}
