package _01_design_principles_and_ideas.part1_OOP.demo19.domain;


import _01_design_principles_and_ideas.part1_OOP.exception.InsufficientBalanceException;
import _01_design_principles_and_ideas.part1_OOP.exception.InvalidAmountException;

import java.math.BigDecimal;




//12|实战一（下）：如何利用基于充血模型的DDD开发一个虚拟钱包系统？
//示例: 基于充血.
//      利用基于充血模型的传统开发模式来实现的虚拟钱包系统
//      将原来在 Service 类中的部分业务逻辑移动到 VirtualWallet 类中，
//      让 Service 类的实现依赖 VirtualWallet 类


// Domain领域模型(充血模型)
public class VirtualWallet {
    private Long id;
    private Long createTime = System.currentTimeMillis();
    ;
    private BigDecimal balance = BigDecimal.ZERO;

    public VirtualWallet(Long preAllocatedId) {
        this.id = preAllocatedId;
    }

    public BigDecimal balance() {
        return this.balance;
    }

    public void debit(BigDecimal amount) throws Exception {
        if (this.balance.compareTo(amount) < 0) {
            throw new InsufficientBalanceException();
        }
        this.balance.subtract(amount);
    }

    public void credit(BigDecimal amount) {
        if (amount.compareTo(BigDecimal.ZERO) < 0) {
            throw new InvalidAmountException();
        }
        this.balance.add(amount);
    }
}

