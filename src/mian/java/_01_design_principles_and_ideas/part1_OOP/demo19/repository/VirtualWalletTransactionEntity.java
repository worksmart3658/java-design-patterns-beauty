package _01_design_principles_and_ideas.part1_OOP.demo19.repository;

import _01_design_principles_and_ideas.part1_OOP.demo18.service.Status;

import java.math.BigDecimal;

public class VirtualWalletTransactionEntity {
    public void setAmount(BigDecimal amount) {

    }

    public void setCreateTime(long currentTimeMillis) {

    }

    public void setFromWalletId(Long fromWalletId) {

    }

    public void setToWalletId(Long toWalletId) {

    }

    public void setStatus(Status toBeExecuted) {

    }
}
