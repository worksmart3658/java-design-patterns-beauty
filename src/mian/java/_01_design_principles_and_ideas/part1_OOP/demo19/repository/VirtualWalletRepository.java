package _01_design_principles_and_ideas.part1_OOP.demo19.repository;

import java.math.BigDecimal;

public class VirtualWalletRepository {
    public VirtualWalletEntity getWalletEntity(Long walletId) {
        return null;
    }

    public BigDecimal getBalance(Long walletId) {
        return null;
    }

    public void updateBalance(Long walletId, BigDecimal subtract) {

    }
}
