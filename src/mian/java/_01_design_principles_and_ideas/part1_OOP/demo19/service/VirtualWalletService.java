package _01_design_principles_and_ideas.part1_OOP.demo19.service;

import _01_design_principles_and_ideas.part1_OOP.demo19.domain.VirtualWallet;
import _01_design_principles_and_ideas.part1_OOP.demo19.repository.VirtualWalletEntity;
import _01_design_principles_and_ideas.part1_OOP.demo19.repository.VirtualWalletRepository;
import _01_design_principles_and_ideas.part1_OOP.demo19.repository.VirtualWalletTransactionRepository;

import java.math.BigDecimal;

/**
 * @ClassName VirtualWalletService
 * @Description
 * @Author zhanghuaxing
 * @Date 11/9/2020 12:39 PM
 **/
public class VirtualWalletService {
    // 通过构造函数或者IOC框架注入
    private VirtualWalletRepository walletRepo;
    private VirtualWalletTransactionRepository transactionRepo;

    public VirtualWallet getVirtualWallet(Long walletId) {
        VirtualWalletEntity walletEntity = walletRepo.getWalletEntity(walletId);
        VirtualWallet wallet = convert(walletEntity);
        return wallet;
    }

    private VirtualWallet convert(VirtualWalletEntity walletEntity) {
        return null;
    }

    public BigDecimal getBalance(Long walletId) {
        return walletRepo.getBalance(walletId);
    }

    public void debit(Long walletId, BigDecimal amount) throws Exception {
        VirtualWalletEntity walletEntity = walletRepo.getWalletEntity(walletId);
        VirtualWallet wallet = convert(walletEntity);
        wallet.debit(amount);
        walletRepo.updateBalance(walletId, wallet.balance());
    }

    public void credit(Long walletId, BigDecimal amount) {
        VirtualWalletEntity walletEntity = walletRepo.getWalletEntity(walletId);
        VirtualWallet wallet = convert(walletEntity);
        wallet.credit(amount);
        walletRepo.updateBalance(walletId, wallet.balance());
    }

    public void transfer(Long fromWalletId, Long toWalletId, BigDecimal amount) {
        //...跟基于贫血模型的传统开发模式的代码一样...
    }
}

