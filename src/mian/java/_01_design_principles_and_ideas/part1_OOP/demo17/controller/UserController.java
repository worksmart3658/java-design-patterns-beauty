package _01_design_principles_and_ideas.part1_OOP.demo17.controller;

import _01_design_principles_and_ideas.part1_OOP.demo17.service.UserBo;
import _01_design_principles_and_ideas.part1_OOP.demo17.service.UserService;


//11|实战一（上）：业务开发常用的基于贫血模型的MVC架构违背OOP吗？
// 对比:
//      1.基于贫血模型的传统的开发模式
//      2.基于充血模型的 DDD 开发模式

//示例:贫血模型

////////// Controller+VO(View Object) //////////
public class UserController {
    private UserService userService; //通过构造函数或者IOC框架注入

    public UserVo getUserById(Long userId) {
        UserBo userBo = userService.getUserById(userId);
//        UserVo userVo = [...convert userBo to userVo...];
        UserVo userVo = convertUserBoToUserVo(userBo);
        return userVo;
    }

    private UserVo convertUserBoToUserVo(UserBo userBo) {
        return null;
    }
}
