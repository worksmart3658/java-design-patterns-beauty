package _01_design_principles_and_ideas.part1_OOP.demo17.service;

import _01_design_principles_and_ideas.part1_OOP.demo17.repository.UserEntity;
import _01_design_principles_and_ideas.part1_OOP.demo17.repository.UserRepository;

////////// Service+BO(Business Object) //////////
public class UserService {
    private UserRepository userRepository; //通过构造函数或者IOC框架注入

    public UserBo getUserById(Long userId) {
        UserEntity userEntity = userRepository.getUserById(userId);
        //UserBo userBo = [...convert userEntity to userBo...];
        UserBo userBo = convertUserEntityToUserBo(userEntity);
        return userBo;
    }

    private UserBo convertUserEntityToUserBo(UserEntity userEntity) {
        return null;
    }
}
